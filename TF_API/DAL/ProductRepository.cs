﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TF_API.DAL.DTO;
using TF_API.DAL.EF;
using TF_API.MapperExtension;

namespace TF_API.DAL
{
    public class ProductRepository
    {
        public ProductDTO Get(int id)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                Product p = db.Products.FirstOrDefault(u => u.ID == id);
                return p.Map<ProductDTO>();
            }
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                List<ProductDTO> result = new List<ProductDTO>();
                foreach (Product item in db.Products)
                {
                    result.Add(item.Map<ProductDTO>());
                }
                return result;
            }
        }

        public int Insert(ProductDTO p)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                var prod = p.Map<Product>();
                db.Products.Add(prod);
                db.SaveChanges();
                return prod.ID;
            }
        }

        public void Update(ProductDTO us)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                Product u1 = db.Products.FirstOrDefault(u => u.ID == us.ID);
                u1.ExpirationDate = us.ExpirationDate;
                u1.Name = us.Name;
                u1.Price = us.Price;
                u1.Quantity = us.Quantity;
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                Product u1 = db.Products.FirstOrDefault(u => u.ID == id);
                db.Products.Remove(u1);
                db.SaveChanges();
            }
        }
    }
}