﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TF_API.DAL.EF;

namespace TF_API.DAL
{
    public class UserRepository
    {
        public User Get(int id)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                return db.Users.FirstOrDefault(u => u.ID == id);
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                return db.Users;
            }
        }

        public int Insert(User u)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                db.Users.Add(u);
                db.SaveChanges();
                return u.ID;
            }
        }

        public void Update(User us)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                User u1 = db.Users.FirstOrDefault(u => u.ID == us.ID);
                u1.Password = us.Password;
                u1.Username = us.Username;
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (ApiDBEntities db = new ApiDBEntities())
            {
                User u1 = db.Users.FirstOrDefault(u => u.ID == id);
                db.Users.Remove(u1);
                db.SaveChanges();
            }
        }
    }
}