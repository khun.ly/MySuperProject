﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TF_API.DAL.DTO
{
    public class ProductDTO
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public System.DateTime ExpirationDate { get; set; }
    }
}