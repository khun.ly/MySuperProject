﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TF_API.DAL.DTO
{
    public class UserDTO
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}