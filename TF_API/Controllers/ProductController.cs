﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TF_API.Authorization;
using TF_API.DAL;
using TF_API.DAL.DTO;

namespace TF_API.Controllers
{
    [AuthorizeAPI]
    public class ProductController : ApiController
    {
        // GET api/Product
        public IEnumerable<ProductDTO> Get()
        {
            ProductRepository repo = new ProductRepository();

            return repo.GetAll() ;
        }

        // GET api/values/5
        public ProductDTO Get(int id)
        {
            ProductRepository repo = new ProductRepository();

            return repo.Get(id);
        }

        // POST api/values
        public void Post([FromBody]ProductDTO value)
        {
            ProductRepository repo = new ProductRepository();

            repo.Insert(value);
        }


        // PUT api/values/5
        public void Put([FromBody]ProductDTO value)
        {
            ProductRepository repo = new ProductRepository();

            repo.Update(value);
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            ProductRepository repo = new ProductRepository();

            repo.Delete(id);
        }
    }
}
