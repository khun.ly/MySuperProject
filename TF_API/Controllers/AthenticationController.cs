﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TF_API.Authorization;
using TF_API.DAL.DTO;
using TF_API.DAL.EF;
using Autofac;

namespace TF_API.Controllers
{
    public class AthenticationController : ApiController
    {
        [HttpPost]
        [Route("Api/Login")]
        public string Login([FromBody]UserDTO us)
        {

            using (ApiDBEntities db = new ApiDBEntities())
            {
                User u1 = db.Users
                    .FirstOrDefault(u => u.Username == us.Username && u.Password == us.Password);
                if (u1 != null)
                {
                    ITokenEncoder e 
                        = WebApiApplication.container
                        .Resolve<ITokenEncoder>();
                    us.ID = u1.ID;
                    return e.Encode(us);
                }
                return null;
            }
            
            
        }
    }
}
