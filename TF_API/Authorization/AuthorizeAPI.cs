﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using Autofac;

namespace TF_API.Authorization
{
    public class AuthorizeAPIAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            IEnumerable<string> AuthHeaders;
            filterContext.Request.Headers
                .TryGetValues("Authorization", out AuthHeaders);
            if (AuthHeaders != null)
            {
                string token = AuthHeaders.First();
                ITokenEncoder encoder
                        = WebApiApplication.container.Resolve<ITokenEncoder>();
                try
                {
                    if (!encoder.Decode(token))
                    {
                        filterContext.Response
                            = new HttpResponseMessage
                            { StatusCode = HttpStatusCode.Unauthorized };
                    }
                }
                catch
                {
                    filterContext.Response
                            = new HttpResponseMessage
                            { StatusCode = HttpStatusCode.Unauthorized };
                }
            }
            else
            {
                filterContext.Response
                        = new HttpResponseMessage
                        { StatusCode = HttpStatusCode.Unauthorized };
            }
        }
    }
}