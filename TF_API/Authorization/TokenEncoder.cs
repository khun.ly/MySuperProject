﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using TF_API.DAL.DTO;
using TF_API.DAL.EF;

namespace TF_API.Authorization
{
    public class TokenEncoder : ITokenEncoder
    {
        private readonly JwtSecurityTokenHandler _Handler;

        //Injection de dépendance
        public TokenEncoder(JwtSecurityTokenHandler handler)
        {
            _Handler = handler;
        }
        private const string securityKey 
            = "fsdfgsdgkldfgoidhfdfsdffsdfsdfdf";
        public bool Decode(string tkn)
        {
            if(!_Handler.CanReadToken(tkn)){
                return false;
            }
            JwtSecurityToken token = _Handler.ReadJwtToken(tkn);
            JwtPayload payload = token.Payload;
            JwtHeader header
                    = new JwtHeader(
                        new SigningCredentials(
                            new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(securityKey)
                                ),
                            SecurityAlgorithms.HmacSha256));
            JwtSecurityToken Comparetoken = new JwtSecurityToken(header, payload);
            return (tkn == _Handler.WriteToken(Comparetoken));

        }

        public string Encode(UserDTO u)
        {
            JwtHeader header
                = new JwtHeader(
                    new SigningCredentials(
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(securityKey)
                            ), 
                        SecurityAlgorithms.HmacSha256));
            JwtPayload payload = new JwtPayload()
            {
                { "Username" , u.Username }, { "ID" , u.ID }
            };
            JwtSecurityToken token= new JwtSecurityToken(header, payload);
                    
 
            string stringToken = _Handler.WriteToken(token); 
            return stringToken;
        }
    }
}