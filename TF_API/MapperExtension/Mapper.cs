using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TF_API.MapperExtension
{
    public static class Mapper
    {
        public static T Map<T>(this object o)
            where T: class
        {
            Type typeFrom = o.GetType();
            Type typeTo = typeof(T);
            var propFrom = typeFrom.GetProperties().Where(p => p.CanRead);
            ConstructorInfo ctor = typeTo.GetConstructor(new Type[0]);
            T instance = ctor?.Invoke(new object[0]) as T;
            foreach (var prop in propFrom)
            {
                var propA = typeTo.GetProperty(prop.Name);
                if (propA != null && propA.CanWrite)
                {
                    var value = prop.GetValue(o);
                    propA.SetValue(instance, value);
                }
            }
			
			/////fsdfdsgdfgdfsgdfgs
            //add comment




            //commentaires
            //commentaires
            //commentaires
            //commentaires
            //commentaires
            //commentaires
            //commentaires
            //commentaires
            //commentaires


            //fcsgfdg

			//Changements faits par 2


            return instance;
        }
    }
}